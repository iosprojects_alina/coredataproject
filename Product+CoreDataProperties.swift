//
//  Product+CoreDataProperties.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 1/27/16.
//  Copyright © 2016 dimalina. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Product {

    @NSManaged var title: String?
    @NSManaged var image: NSData?
    @NSManaged var store: String?

}
