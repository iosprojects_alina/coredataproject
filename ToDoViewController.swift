//
//  ToDoViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 1/14/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import CoreData

class ToDoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    var tField = UITextField!()
    var items: [Item] = []
    var fetchResultController:NSFetchedResultsController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        

        let request = NSFetchRequest(entityName: "Item")
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        fetchResultController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchResultController.delegate = self
        
        do {
            try self.fetchResultController.performFetch()
        } catch _ {
            fetchResultController = nil
            
        }
        
        if fetchResultController != nil {
            self.items = fetchResultController.fetchedObjects as! [Item]
        }
        
        self.tableView.reloadData()

        // Do any additional setup after loading the view.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let request = NSFetchRequest(entityName: "Item")
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        fetchResultController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        
        
        fetchResultController.delegate = self
        
        do {
            try self.fetchResultController.performFetch()
            
        } catch _ {
            fetchResultController = nil
            
        }
        
        if fetchResultController != nil {
            self.items = fetchResultController.fetchedObjects as! [Item]
        }
        
        self.tableView.reloadData()
        
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let item = self.items[indexPath.row]
        cell.textLabel!.text = item.title
        if item.checked == true {
            cell.imageView!.image = UIImage(named:"checkbox.png")
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: (cell.textLabel!.text)!)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            
            cell.textLabel!.attributedText = attributeString
            cell.textLabel!.textColor = UIColor.grayColor()
            
        } else {
            cell.imageView!.image = UIImage(named:"unchecked.png")
        }
        
        return cell
        
    }
    
    func configurationTextField(textField: UITextField){
    textField.placeholder = "Enter New Item"
    self.tField = textField
    }

    @IBAction func addButtonPressed(sender: AnyObject) {
        alertPopup()
    }
    
    func saveNewItem(){
        
       let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let item = NSEntityDescription.insertNewObjectForEntityForName("Item", inManagedObjectContext: context) as! Item
        
        item.title = tField.text
        
        do {
            try context.save()
        } catch {
            fatalError("Failure to save context:\(error)")
        }
    
        let request = NSFetchRequest(entityName: "Item")
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            try self.fetchResultController.performFetch()
            
        } catch _ {
            fetchResultController = nil
            
        }
        
        if fetchResultController != nil {
            self.items = fetchResultController.fetchedObjects as! [Item]
        }
        self.tableView.reloadData()
    }
    
    func alertPopup() {
        let alert = UIAlertController(title: "Add new Item", message: nil, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in alert.dismissViewControllerAnimated(true, completion: nil)
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.saveNewItem()
        }
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
        
      
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
        //let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let checkedItem = self.fetchResultController.objectAtIndexPath(indexPath) as! Item
        
                    let cell = tableView.cellForRowAtIndexPath(indexPath)
    
                    cell?.imageView?.image = UIImage(named:"checkbox.png")
        
                    checkedItem.checked = true
        
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: (cell?.textLabel?.text)!)
                    attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        
                    cell?.textLabel?.attributedText = attributeString
                    cell?.textLabel?.textColor = UIColor.grayColor()
        
        do {
            try self.fetchResultController.performFetch()
            
        } catch _ {
            fetchResultController = nil
            
        }
        
        if fetchResultController != nil {
            self.items = fetchResultController.fetchedObjects as! [Item]
        }

       
 
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
       
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            let context  = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
            let itemToDelete = self.fetchResultController.objectAtIndexPath(indexPath) as! Item
            
            context.deleteObject(itemToDelete)
            
            
            self.items.removeAtIndex(indexPath.row)
            
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            do {
                try self.fetchResultController.performFetch()
                
            } catch _ {
                fetchResultController = nil
                
            }
            
            if fetchResultController != nil {
                self.items = fetchResultController.fetchedObjects as! [Item]
            }

            
            
        }
        
    }


}
