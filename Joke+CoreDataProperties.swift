//
//  Joke+CoreDataProperties.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 2/19/16.
//  Copyright © 2016 dimalina. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Joke {

    @NSManaged var title: String?
    @NSManaged var text: String?
    @NSManaged var collection: Collection?

}
