//
//  FriendDetailsViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 12/24/15.
//  Copyright © 2015 dimalina. All rights reserved.
//

import UIKit

class FriendDetailsViewController: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
   
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet var ViewCell: UIView!
    var friend = FriendsList()
 
    var cellColor = UIColor()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.labelName.text = self.friend.name
        self.birthdayLabel.text = self.friend.birthday
        self.phoneLabel.text = self.friend.phone
        self.imageView.image = UIImage(named: self.friend.image)
        //self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.imageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.ViewCell.backgroundColor = cellColor

        // Do any additional setup after loading the view.
    }


    

}
