//
//  DetailViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 1/27/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var product: Product? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = self.product?.title
        self.storeLabel.text = self.product?.store
        self.imageView.image = UIImage(data: self.product!.image!)
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        

        // Do any additional setup after loading the view.
    }


}
