//
//  JokeBankViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 2/19/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import CoreData
import StoreKit

class JokeBankViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    @IBOutlet weak var tableView: UITableView!
    var collections = [Collection]()
    
    var products = [SKProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let collectionRequest = NSFetchRequest(entityName: "Collection")
        
        do{
            
            try self.collections = context.executeFetchRequest(collectionRequest) as! [Collection]
            if self.collections.count <= 0 {
                fillJokeBank()
            }
        } catch {
            
        }
        
        grabCollection()
        prepateForPurchase()
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        

        // Do any additional setup after loading the view.
    }

    func prepateForPurchase(){
        
        let productSet : Set<String> = ["test1", "test2"]
        let request = SKProductsRequest(productIdentifiers: productSet)
        request.delegate = self
        request.start()
        
    
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        print ("products: \(response.products.count)")
        print("invalid: \(response.invalidProductIdentifiers.count)")
        self.products = response.products
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let collection = self.collections[indexPath.row]
        
        if collection.inAppPurchaseID?.isEmpty == nil {
            cell.textLabel!.text = collection.title
        
        } else {
            if collection.purchased!.boolValue {
                cell.textLabel!.text = collection.title
                } else {
            
            var currentProduct: SKProduct?
            for product in self.products {
                if product.productIdentifier == collection.inAppPurchaseID {
                    currentProduct = product
                }
                }
            
            //that will give valid product title and price right from iTunes where identifiers must be setup
            if currentProduct != nil {
                
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .CurrencyStyle
                formatter.locale = currentProduct?.priceLocale
                
                //let priceString = formatter.stringFromNumber((currentProduct?.price)!)
                cell.textLabel!.text = "Locked"// * \(collection.title!) * \(priceString!)"
            } else {
                cell.textLabel!.text = "Locked1"
                }
            }
        
        }
        return cell
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .Purchased:
                print("Purchased")
                break
            case .Failed:
                print("Failed")
                giveRewardForProduct(transaction.payment.productIdentifier)
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                break
            case .Restored:
                print("Restored")
                break
            case .Purchasing:
                print("Purchasing")
                break
            case .Deferred:
                print("Deferred")
                break
            }
        }
    }
    
    func giveRewardForProduct (productID:String){
        for collection in self.collections {
            if productID == collection.inAppPurchaseID {
                collection.purchased = true
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.collections.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let collection = collections[indexPath.row]
        if collection.purchased!.boolValue {
        self.performSegueWithIdentifier("CollectionToJokeSeque", sender: collection)
        } else {
            print("purchased")
            collection.purchased = true
            self.tableView.reloadData()
        
            //checking
//            var currentProduct: SKProduct?
//            for product in self.products {
//                if product.productIdentifier == collection.inAppPurchaseID {
//                    currentProduct = product
//                }
//            }
//            let payment = SKPayment(product: currentProduct!)
//            SKPaymentQueue.defaultQueue().addPayment(payment)
            
        }
        }


    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let jokeVC = segue.destinationViewController as! JokesViewController
        jokeVC.collection = sender as? Collection
        
    }
    
    func grabCollection(){
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let collectionRequest = NSFetchRequest(entityName: "Collection")
        
        do{
        
            try self.collections = context.executeFetchRequest(collectionRequest) as! [Collection]
            self.tableView.reloadData()
        } catch {
            
        }
        
    }
    func fillJokeBank() {
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let collectionDescription = NSEntityDescription.entityForName("Collection", inManagedObjectContext: context)
        
        let jokeDescription = NSEntityDescription.entityForName("Joke", inManagedObjectContext: context)
        
        //Collections
        let collection1 = Collection(entity: collectionDescription!, insertIntoManagedObjectContext: context)
        collection1.title = "random joke to check"
        collection1.purchased = true
        
        let collection2 = Collection(entity: collectionDescription!, insertIntoManagedObjectContext: context)
        collection2.title = "test1"
        collection2.purchased = false
        collection2.inAppPurchaseID = "test1"
        
        let collection3 = Collection(entity: collectionDescription!, insertIntoManagedObjectContext: context)
        collection3.title = "test2"
        collection3.purchased = false
        collection3.inAppPurchaseID = "test2"
        
        
        //Jokes
        let joke1 = Joke(entity: jokeDescription!, insertIntoManagedObjectContext: context)
        joke1.title = "Taxi"
        joke1.text = "Bla-Bla-BlaBla-Bla-BlaBla-Bla-BlaBla-Bla-BlaBla-Bla-BlaBla-Bla-BlaBla-Bla-BlaBla-Bla-Bla"
        
        joke1.collection = collection1
        
        let joke2 = Joke(entity: jokeDescription!, insertIntoManagedObjectContext: context)
        joke2.title = "Lame"
        joke2.text = "this is Lame"
        
        joke2.collection = collection2
        
        
        let joke3 = Joke(entity: jokeDescription!, insertIntoManagedObjectContext: context)
        joke3.title = "This is change to test repo"
        joke3.text = "the lamiest thing ever"
        
        joke3.collection = collection3
        
        do {
            try context.save()
        } catch {
        
        }
    }
   }


