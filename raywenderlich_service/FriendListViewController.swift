//
//  FriendListViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 12/23/15.
//  Copyright © 2015 dimalina. All rights reserved.
//

import UIKit


class FriendListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!

    let friends = ["jessica", "dima", "cooper", "test"]
    var color = UIColor()
    
    var friendslist:[FriendsList] = [FriendsList(name: "jessy", birthday: "10th June", phone: "122-123-5674", image: "alya.jpg"), FriendsList(name: "dima", birthday: "11th June", phone: "122-123-5674", image: "test.jpg"), FriendsList(name: "alina", birthday: "12th June", phone: "122-123-5674", image: "alya.jpg"), FriendsList(name: "test", birthday: "13th June", phone: "122-123-5674", image: "test.jpg"), FriendsList(name: "test", birthday: "13th June", phone: "122-123-5674", image: "test.jpg")]
    
    let colors:[UIColor] =  [UIColor(red: 255/255, green: 0/255, blue: 128/255, alpha: 1.0), UIColor.blueColor(), UIColor.greenColor(), UIColor.purpleColor()]
    
    var dictionaryExamples = ["test1": "1", "test2":"2"] as [String: AnyObject]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictionaryExamples["test3"]="3"
        dictionaryExamples["test4"]="7"
        dictionaryExamples["age"] = 56
        
        print("\(dictionaryExamples)")

        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendslist.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        let colorIndex = indexPath.row % colors.count
        cell.backgroundColor = self.colors[colorIndex]
        
        let friend = friendslist[indexPath.row]
        cell.textLabel!.text = friend.name
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        let friend = friendslist[indexPath.row]
        let colorIndex = indexPath.row % colors.count
        color = self.colors[colorIndex]
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("FriendListToFrieldDetailSegue", sender: friend)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detailViewController = segue.destinationViewController as! FriendDetailsViewController
        detailViewController.friend = sender as! FriendsList
        detailViewController.cellColor = color
        
    }
}
