//
//  NewProductViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 1/27/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import CoreData

class NewProductViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var storeTextField: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageTapRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped")
        self.imageView.addGestureRecognizer(imageTapRecognizer)

        // Do any additional setup after loading the view.
    }

    func imageTapped(){
        
        
        let optionMenu = UIAlertController(title: nil, message: "Choose the source", preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        optionMenu.addAction(cancelAction)
        
        let CameraAction = UIAlertAction(title: "Camera", style: .Default, handler: {
            
            (action:UIAlertAction!) -> Void in self.getPhotoFromSource(UIImagePickerControllerSourceType.Camera)})
        optionMenu.addAction(CameraAction)
        
        let photoLibrary = UIAlertAction(title: "Library", style: .Default, handler: {
            
            (action:UIAlertAction!) -> Void in self.getPhotoFromSource(UIImagePickerControllerSourceType.PhotoLibrary)})
        optionMenu.addAction(photoLibrary)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    
    }
    
    
    func getPhotoFromSource(source:UIImagePickerControllerSourceType ){
    
        if UIImagePickerController.isSourceTypeAvailable(source){
            let imagePicker = UIImagePickerController()
            imagePicker.modalPresentationStyle = .CurrentContext
            imagePicker.delegate = self
            imagePicker.sourceType = source
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
    
    }
    

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.imageView.image = info [UIImagePickerControllerOriginalImage] as? UIImage
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        imageView.clipsToBounds = true
        picker.dismissViewControllerAnimated(true, completion: nil)
        picker.delegate = nil
    }
    @IBAction func cancelTapped(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func saveTapped(sender: AnyObject) {
        
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let product = NSEntityDescription.insertNewObjectForEntityForName("Product", inManagedObjectContext: context) as! Product
        product.title = titleTextField.text
        product.store = storeTextField.text
        product.image = UIImageJPEGRepresentation((self.imageView!.image!),1.0)
        do {
            try context.save()
        } catch _ {}
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
