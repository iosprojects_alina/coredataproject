//
//  WishlistViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 1/26/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import CoreData

class WishlistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var products: [Product] = []
    
    var selectedProduct: Product? = nil
    
    var fetchResultController:NSFetchedResultsController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        
        let request = NSFetchRequest(entityName: "Product")
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        fetchResultController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchResultController.delegate = self
        
        do {
            try self.fetchResultController.performFetch()
            //results =  try context.executeFetchRequest(request)

        } catch _ {
            fetchResultController = nil
            //results = nil
        
        }
        
        if fetchResultController != nil {
            self.products = fetchResultController.fetchedObjects as! [Product]
        }

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        let request = NSFetchRequest(entityName: "Product")
        //var results: [AnyObject]?
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        fetchResultController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchResultController.delegate = self
        
        do {
            try self.fetchResultController.performFetch()
            //results =  try context.executeFetchRequest(request)
            
        } catch _ {
            fetchResultController = nil
            //results = nil
            
        }
        
        if fetchResultController != nil {
            self.products = fetchResultController.fetchedObjects as! [Product]
        }
        
        self.tableView.reloadData()

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        let product = products[indexPath.row]
        cell.textLabel!.text = product.title
        cell.imageView!.image = UIImage(data: product.image!)
        cell.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
    
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.selectedProduct = products[indexPath.row]
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("tableViewToDetailSeque", sender: selectedProduct)
    } 

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "tableViewToDetailSeque"{
            let detailViewController = segue.destinationViewController as! DetailViewController
            detailViewController.product = self.selectedProduct
        }
        
    }
    
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            let context  = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
            let productToDelete = self.fetchResultController.objectAtIndexPath(indexPath) as! Product
            
            context.deleteObject(productToDelete)

            
            self.products.removeAtIndex(indexPath.row)
            
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                
            
        }
        
    }
    
   }
