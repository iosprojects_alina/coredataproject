//
//  FriendsList.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 12/25/15.
//  Copyright © 2015 dimalina. All rights reserved.
//

import Foundation

class FriendsList{
    var name: String = ""
    var birthday: String = ""
    var phone: String = ""
    var image: String = ""
    
    init (name:String, birthday:String, phone:String, image:String){
        self.name = name
        self.birthday = birthday
        self.phone = phone
        self.image = image
    }
    
    init(){}
    
}