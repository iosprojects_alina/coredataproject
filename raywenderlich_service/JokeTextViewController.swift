//
//  JokeTextViewController.swift
//  raywenderlich_service
//
//  Created by Alina Chernenko on 3/9/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit

class JokeTextViewController: UIViewController {

    @IBOutlet weak var testView: UITextView!
    
    var joke : Joke?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.testView.text = self.joke?.text

        
        // Do any additional setup after loading the view.
    }


}
